﻿using System;
using System.Threading.Tasks;

namespace Reminders.Core.Extensions
{
    public static class TaskExtension
    {
        public async static void Await(this Task task, Action completedCallBack = null, Action<Exception> errorsCallBack = null)
        {
            try
            {
                await task;
                completedCallBack?.Invoke();
            }
            catch (Exception exception)
            {
                errorsCallBack?.Invoke(exception);
            }
        }
    }
}
