﻿namespace Reminders
{
    public static class GlobalRegions
    {
        public const string ChromeRegion = "ChromeRegion";
        public const string ContentRegion = "ContentRegion";
        public const string BottomRegion = "BottomRegion";
    }
}
